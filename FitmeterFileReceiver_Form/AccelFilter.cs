﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitmeterFileReceiver_Form
{
    public class AccelFilter
    {
        private short[][] filterBuffer;
        private int filterBufferIndex;
        private int filter_herz = 250;
        private float filter_seconds = 2;
        private int filterBufferSize = 500;
        private int[] sum;

        public AccelFilter(short[] initValues, float seconds=2, int herz=250)
        {
            this.filter_herz = herz;
            this.filter_seconds = seconds;
            filterBufferSize =(int) (herz * seconds);
            initFilter(initValues);
        }


        private void initFilter(short[] firstValue)
        {
            filterBuffer = new short[3][];

            filterBuffer[0] = new short[filterBufferSize];
            filterBuffer[1] = new short[filterBufferSize];
            filterBuffer[2] = new short[filterBufferSize];
            filterBufferIndex = 0;

            for (int i = 0; i < filterBufferSize; i++)
            {
                filterBuffer[0][i] = firstValue[0];
                filterBuffer[1][i] = firstValue[1];
                filterBuffer[2][i] = firstValue[2];
            }
            sum = new int[3];
            sum[0] = firstValue[0] * filterBufferSize;
            sum[1] = firstValue[1] * filterBufferSize;
            sum[2] = firstValue[2] * filterBufferSize;
        }
        public short[] getFilteredAccelValues(short[] accelAxisValues)
        {
            sum[0] -= filterBuffer[0][filterBufferIndex] - accelAxisValues[0];
            sum[1] -= filterBuffer[1][filterBufferIndex] - accelAxisValues[1];
            sum[2] -= filterBuffer[2][filterBufferIndex] - accelAxisValues[2];
            filterBuffer[0][filterBufferIndex] = accelAxisValues[0];
            filterBuffer[1][filterBufferIndex] = accelAxisValues[1];
            filterBuffer[2][filterBufferIndex] = accelAxisValues[2];

            filterBufferIndex++;
            if (filterBufferIndex == filterBufferSize) filterBufferIndex = 0;

            short[] result = new short[3];

            result[0] =(short)( accelAxisValues[0] - ((sum[0] / filterBufferSize)));
            result[1] = (short)(accelAxisValues[1] - ((sum[1] / filterBufferSize)));
            result[2] = (short)(accelAxisValues[2] - ((sum[2] / filterBufferSize)));

            return result;
        }
    }
}
