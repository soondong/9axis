﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitmeterFileReceiver_Form
{
    public class MagnetConpensation{
       

        //분당구 정자동 현재
       // private const double DECLINATION = 14.10;

        //수원시
        private const double DECLINATION = 14.07;
       // private const 14.07
       
 
        public MagnetConpensation(){
            setMagnetValue(new short[] { 0, 0, 0 });   
        }
        float mx=1;
        float my=1;
        float mz=1;
        double heading=0;
        double roll=0;
        double pitch = 0;
        private void setMagnetValue(short[] magnetValues)
        {
            if (magnetValues[0] == mx || magnetValues[1] == my || magnetValues[2] == mz)
            {
                return;
            }
            else
            {
                mx = magnetValues[0];
                my = magnetValues[1];
                mz = magnetValues[2];

                if (my == 0)
                    heading = (mx < 0) ? 180.0 : 0;
                else
                    heading = Math.Atan2(mx, my);

                heading -= DECLINATION * Math.PI / 180;

                if (heading > Math.PI) heading -= (2 * Math.PI);
                else if (heading < -Math.PI) heading += (2 * Math.PI);
                else if (heading < 0) heading += 2 * Math.PI;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="magnetValues"></param>
        /// <param name="accelValues"></param>
        /// <param name="ValuePerG">1G당 몇인지 (8G 세팅일때 1G는 256이므로 256)</param>
        public short[] getAccelValuesWithMagnetFilter(short[] magnetValues, short[] accelValues, int ValuePerG)
        {

            float ax = accelValues[0];
            float ay = accelValues[1];
            float az = accelValues[2];

            setMagnetValue(magnetValues);

             roll = Math.Atan2(accelValues[1], accelValues[2]);
             pitch = Math.Atan2(-ax, Math.Sqrt(ay * ay + az * az));
        //    roll = Math.Atan2(accelValues[1], accelValues[2]);
        //    pitch = Math.Atan2(-ax, Math.Sqrt(ay * ay + az * az));

            short x = (short)(accelValues[0] + ValuePerG * Math.Sin(pitch));
            short y = (short)(accelValues[1] - ValuePerG * Math.Sin(roll));
            short z = (short)(accelValues[2] - ValuePerG * Math.Sin(heading));

            return new short[] {(short) x, (short)y, (short)z };
            

        }


    }
}
