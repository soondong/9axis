﻿namespace FitmeterFileReceiver_Form
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox_connectedDevice = new System.Windows.Forms.GroupBox();
            this.label_battery = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.button_receiveSelectedFiles = new System.Windows.Forms.Button();
            this.button_deletefile = new System.Windows.Forms.Button();
            this.button_fileReceive = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label_serialNumber = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_timeSetting = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.label_time = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox_connectedDevice.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_connectedDevice
            // 
            this.groupBox_connectedDevice.Controls.Add(this.groupBox2);
            this.groupBox_connectedDevice.Controls.Add(this.button3);
            this.groupBox_connectedDevice.Controls.Add(this.groupBox1);
            this.groupBox_connectedDevice.Controls.Add(this.button2);
            this.groupBox_connectedDevice.Controls.Add(this.label_serialNumber);
            this.groupBox_connectedDevice.Controls.Add(this.label3);
            this.groupBox_connectedDevice.Controls.Add(this.label_timeSetting);
            this.groupBox_connectedDevice.Enabled = false;
            this.groupBox_connectedDevice.Location = new System.Drawing.Point(12, 41);
            this.groupBox_connectedDevice.Name = "groupBox_connectedDevice";
            this.groupBox_connectedDevice.Size = new System.Drawing.Size(469, 358);
            this.groupBox_connectedDevice.TabIndex = 0;
            this.groupBox_connectedDevice.TabStop = false;
            this.groupBox_connectedDevice.Text = "연결된 장치";
            // 
            // label_battery
            // 
            this.label_battery.AutoSize = true;
            this.label_battery.Location = new System.Drawing.Point(397, 19);
            this.label_battery.Name = "label_battery";
            this.label_battery.Size = new System.Drawing.Size(11, 12);
            this.label_battery.TabIndex = 16;
            this.label_battery.Text = "-";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(300, 14);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(91, 23);
            this.button7.TabIndex = 15;
            this.button7.Text = "배터리 체크";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label_battery);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.button7);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(17, 64);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(439, 48);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "RTC_Calribration";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(274, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "-";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(171, 12);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(97, 23);
            this.button6.TabIndex = 2;
            this.button6.Text = "Do calibration";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(78, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(87, 21);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "클럭입력:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(310, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(153, 23);
            this.button3.TabIndex = 13;
            this.button3.Text = "펌웨어 업데이트 모드로";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listView1);
            this.groupBox1.Controls.Add(this.button_receiveSelectedFiles);
            this.groupBox1.Controls.Add(this.button_deletefile);
            this.groupBox1.Controls.Add(this.button_fileReceive);
            this.groupBox1.Location = new System.Drawing.Point(10, 118);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(456, 249);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "파일 목록";
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(7, 20);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(439, 188);
            this.listView1.TabIndex = 10;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // button_receiveSelectedFiles
            // 
            this.button_receiveSelectedFiles.Location = new System.Drawing.Point(6, 214);
            this.button_receiveSelectedFiles.Name = "button_receiveSelectedFiles";
            this.button_receiveSelectedFiles.Size = new System.Drawing.Size(128, 23);
            this.button_receiveSelectedFiles.TabIndex = 11;
            this.button_receiveSelectedFiles.Text = "선택파일 받기";
            this.button_receiveSelectedFiles.UseVisualStyleBackColor = true;
            this.button_receiveSelectedFiles.Click += new System.EventHandler(this.button_receiveSelectedFiles_Click);
            // 
            // button_deletefile
            // 
            this.button_deletefile.Location = new System.Drawing.Point(307, 214);
            this.button_deletefile.Name = "button_deletefile";
            this.button_deletefile.Size = new System.Drawing.Size(139, 23);
            this.button_deletefile.TabIndex = 6;
            this.button_deletefile.Text = "파일 전체 삭제";
            this.button_deletefile.UseVisualStyleBackColor = true;
            this.button_deletefile.Click += new System.EventHandler(this.button_deletefile_Click);
            // 
            // button_fileReceive
            // 
            this.button_fileReceive.Enabled = false;
            this.button_fileReceive.Location = new System.Drawing.Point(140, 214);
            this.button_fileReceive.Name = "button_fileReceive";
            this.button_fileReceive.Size = new System.Drawing.Size(139, 23);
            this.button_fileReceive.TabIndex = 2;
            this.button_fileReceive.Text = "파일 모두 받기";
            this.button_fileReceive.UseVisualStyleBackColor = true;
            this.button_fileReceive.Click += new System.EventHandler(this.button_fileReceive_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(10, 35);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(106, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "시간 동기화";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label_serialNumber
            // 
            this.label_serialNumber.AutoSize = true;
            this.label_serialNumber.Location = new System.Drawing.Point(71, 17);
            this.label_serialNumber.Name = "label_serialNumber";
            this.label_serialNumber.Size = new System.Drawing.Size(11, 12);
            this.label_serialNumber.TabIndex = 5;
            this.label_serialNumber.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "장치 ID   :";
            // 
            // label_timeSetting
            // 
            this.label_timeSetting.AutoSize = true;
            this.label_timeSetting.Location = new System.Drawing.Point(155, 40);
            this.label_timeSetting.Name = "label_timeSetting";
            this.label_timeSetting.Size = new System.Drawing.Size(0, 12);
            this.label_timeSetting.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(139, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "CSV로 내보내기";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.label_time);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Location = new System.Drawing.Point(159, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(322, 32);
            this.panel1.TabIndex = 4;
            this.panel1.Visible = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(211, 5);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(105, 23);
            this.button5.TabIndex = 2;
            this.button5.Text = "savefile";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.Location = new System.Drawing.Point(100, 10);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(38, 12);
            this.label_time.TabIndex = 1;
            this.label_time.Text = "label1";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(4, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(90, 25);
            this.button4.TabIndex = 0;
            this.button4.Text = "시간 읽기";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 406);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox_connectedDevice);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Fitmeter 9Axis";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox_connectedDevice.ResumeLayout(false);
            this.groupBox_connectedDevice.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_connectedDevice;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_fileReceive;
        private System.Windows.Forms.Label label_timeSetting;
        private System.Windows.Forms.Label label_serialNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_deletefile;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button button_receiveSelectedFiles;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_battery;
        private System.Windows.Forms.Button button7;

    }
}

