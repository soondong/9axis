﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using com.fitdotlife.fitmeter.protocol_v2;
using System.ComponentModel;
using com.fitdotlife.fitmeter;

namespace FitmeterFileReceiver_Form
{

    public class SerialPortFileReceiver: INotifyPropertyChanged
    {
        SerialPort _serialPort;
        int sizeToRead;
        int fileID;
        Thread serThread;
       

        public SerialPortFileReceiver(SerialPort openedPort, int fileID, int fileSize)
        {
            _serialPort = openedPort;
            sizeToRead = fileSize;
            this.fileID = fileID;
            serThread = new Thread(new ThreadStart(StartToRead));
            serThread.Priority = ThreadPriority.Highest;
            
           
        }

        public void Start()
        {
            
           
            byte[] send = RequestMessage.Request(RequestHeaderCodes.FILEREAD);
            Transmit(send);
            serThread.Start();
        }
        
        private void StartToRead()
        {
            
            ProgressMAX = sizeToRead;
            ProgressValue = 0;
           
            

            int count = 0;
            DateTime _lastRecieved = DateTime.UtcNow;

            DateTime start = DateTime.UtcNow;
            List<byte> receivedBytes = new List<byte>();
            while (count < sizeToRead)
            {
                int bufferCount = _serialPort.BytesToRead;
                if (bufferCount > 0)
                {
                    byte[] buffer = new byte[bufferCount];
                    int receiveCount = Receive(buffer, 0, bufferCount);
                    count += receiveCount;
                //    ProgressValue = count;

                    receivedBytes.AddRange(buffer);
                    _lastRecieved = DateTime.UtcNow;
                }
                DateTime now = DateTime.UtcNow;
                Console.WriteLine(""+ (int)(receivedBytes.Count / (1000 * (new TimeSpan(now.Ticks - start.Ticks).TotalSeconds))));
                TimeSpan ts = new TimeSpan(now.Ticks - _lastRecieved.Ticks);
                if (ts.TotalSeconds > 2)
                {
                    Console.WriteLine("Read Timeout");
                    break;
                   
                }                
            }
            _serialPort.Close();

        }
        public void Transmit(byte[] packet)
        {
            _serialPort.Write(packet, 0, packet.Length);
        }
        public int Receive(byte[] bytes, int offset, int count)
        {
            int readBytes = 0;

            if (count > 0)
            {
                readBytes = _serialPort.Read(bytes, offset, count);
            }

            return readBytes;
        }
        public event EventHandler ReadDataCompleted;
        public void OnReadDataCompleted(ReadDataCompletedEventArg e)
        {
            if (ReadDataCompleted != null)
            {
                ReadDataCompleted(this, e);
            }
        }
        void Notify(string proName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(proName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private int progressValue = 0;
        public int ProgressValue
        {
            get { return progressValue; }
            set
            {
                if (progressValue > progressMAX)
                {
                    ProgressMAX = progressValue;
                }
                progressValue = value;
                Notify("ProgressValue");
            }
        }
        private int progressMAX = 100;
        public int ProgressMAX
        {
            get { return progressMAX; }
            set
            {
                progressMAX = value;
                Notify("ProgressMAX");
            }
        }
    }
}
