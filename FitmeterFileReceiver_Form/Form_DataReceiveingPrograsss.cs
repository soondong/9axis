﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FitmeterWindowsAgent
{
    public partial class Form_DataReceiveingPrograss : Form
    {
       
        //public Form_DataReceiveingPrograss()
        //{
        //    InitializeComponent();
        //}

        public Form_DataReceiveingPrograss(int pregressMax=100)
        {
            InitializeComponent();
            
            this.progressBar1.Maximum = pregressMax;
            this.progressBar1.Value = 0;
            preTime = DateTime.UtcNow;
        }

        
        DateTime preTime = DateTime.UtcNow;

        internal void SetFileIndex(int p)
        {
            try
            {
                this.Invoke(new MethodInvoker(
                 delegate()
                 {
                     richTextBox1.AppendText("" + p + " 다운로드 시작\n");
                     richTextBox1.SelectionStart = richTextBox1.Text.Length;
                     richTextBox1.ScrollToCaret();
                     return;

                 }));
            }
            catch (Exception)
            {
                ;
            }
        }


        internal void SetPrograssValue(int p)
        {
            try
            {
                this.Invoke(new MethodInvoker(
                 delegate()
                 {
                     if (p > progressBar1.Maximum) 
                     {
                         return;
                     }
                     else
                     {
                         if (progressBar1.Value == 0)
                         {
                             preTime = DateTime.UtcNow;
                            
                         }
                         progressBar1.Value = p;
                         DateTime curr = DateTime.UtcNow;
                         TimeSpan ts = new TimeSpan(curr.Ticks - preTime.Ticks);
                         label1.Text = "" + (int)Math.Round(p / 1000.0 / (ts.TotalSeconds))+" kbyte/sec";
                        
                        
                        

                         return;
                     }
                     
                 }));
            }
            catch (Exception)
            {
                ;
            }
        }
        internal void SetPrograssMAX(int p)
        {
            try
            {
                this.Invoke(new MethodInvoker(
               delegate()
               {
                   progressBar1.Maximum = p;
               }));
            }
            catch (Exception)
            {
                ;
            }
        }
    }
}
