﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitmeterFileReceiver_Form
{
    public class Gravityfilter
    {
        private float alpha;
        private float[] gravity;

        public Gravityfilter(float timeConstant, float dt, short[] initValue)
        {
             alpha = timeConstant / (timeConstant + dt);
            //alpha = 0.8f;
            gravity = new float[3];
            gravity[0] = initValue[0];
            gravity[1] = initValue[1];
            gravity[2] = initValue[2];
        }

        public short[] getFilteredAccelValues(short[] accelValue)
        {
            gravity[0] = alpha * gravity[0] + (1 - alpha) * accelValue[0];
            gravity[1] = alpha * gravity[1] + (1 - alpha) * accelValue[1];
            gravity[2] = alpha * gravity[2] + (1 - alpha) * accelValue[2];

            return new short[] { (short) (accelValue[0]- gravity[0]), (short)(accelValue[1] - gravity[1]), (short)(accelValue[2] - gravity[2]) };
        }
    }
}
