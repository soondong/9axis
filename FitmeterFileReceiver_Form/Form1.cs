﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using com.fitdotlife.fitmeter;
using FitmeterWindowsAgent;
using System.Threading;
using Multimedia;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using com.fitdotlife.fitmeter.protocol_v2;


namespace FitmeterFileReceiver_Form
{
    public partial class Form1 : Form
    {


        private string saveFilePath;
        private const int HERZ = 250;

        Fitmeter ConnectedDevice = null;
        private Form_DataReceiveingPrograss receivingProgressForm = null;
        public Form1()
        {
            InitializeComponent();
            FitmeterPortWatcher.PortsChanged += SerialPortService_PortsChanged;

#if CALIB
            groupBox2.Visible = true;
#else
            groupBox2.Visible = false;
#endif

#if AXIS_9
            this.Text = "Fitmeter 9Axis";
#endif
            listView1.FullRowSelect = true;
            listView1.View = View.Details;
            ListViewExtender extender = new ListViewExtender(listView1);
            // extend 2nd column
            ListViewButtonColumn buttonAction = new ListViewButtonColumn(2);

            buttonAction.Click += OnButtonActionClick;
            buttonAction.FixedWidth = true;

            extender.AddColumn(buttonAction);

            listView1.BeginUpdate();

            // 뷰모드 지정

            listView1.Columns.Add("파일 인덱스", 100);
            listView1.Columns.Add("파일 크기", 200);
            listView1.Columns.Add("전송", 100);

            listView1.EndUpdate();

#if SPEEDTEST
            panel1.Visible = true;
#endif
            //int CALM = 0;
            //bool CALP = true;

            //for (int i = 32768; ; i--)
            //{
            //    getCalibrationValues(i, out CALM, out CALP);
            //    if (CALM <0)
            //    {
            //        Console.WriteLine(i);
            //        break;
            //    }
            //}

               

            //Console.WriteLine("");

        }

        private void readAFileTest(SerialPort openPort, int fileID, int size)
        {
            SerialPortFileReceiver receiver = new SerialPortFileReceiver(openPort, fileID, size);
            receiver.PropertyChanged += ConnectedDevice_PropertyChanged;
            receiver.Start();
        }
        volatile int readCount = 0;
        private void OnButtonActionClick(object sender, ListViewColumnMouseEventArgs e)
        {
            if (ConnectedDevice != null && ConnectedDevice.HasData)
            {
                if (receivingProgressForm == null) receivingProgressForm = new Form_DataReceiveingPrograss();

                receivingProgressForm.Text = "read file";
                receivingProgressForm.Owner = this;
                receivingProgressForm.TopLevel = true;
                receivingProgressForm.Show();
                //this.BeginInvoke(new MethodInvoker(receivingProgressForm.Show));


                Thread.Sleep(1000);
                try
                {
                    int index = Int32.Parse(e.Item.Text);
                    int size = Int32.Parse(e.Item.SubItems[1].Text);

                    receivingProgressForm.SetPrograssMAX(size);
                    receivingProgressForm.SetPrograssValue(0);

                    //SerialPort port = new SerialPort(ConnectedDevice.PortNum);
                    //port.Open(); 
                    //byte[] send = RequestMessage.FileOpen(index);

                    //port.Write(send, 0, send.Length);
                    //List<byte> receive = readSerialPacket(port);
                    //byte[] send1 = RequestMessage.Request(RequestHeaderCodes.FILEREAD);
                    //port.Write(send1, 0, send1.Length);





                    //        readAFileTest(openedSerialPort, index, size);
                    // MessageBox.Show(this, @"you clicked " + e.SubItem.Text);


                    List<FileInfoToRead> readFiles = new List<FileInfoToRead>();
                    readFiles.Add(new FileInfoToRead(index, size));
                    requestedFilesInfo = readFiles;
                    ConnectedDevice.ReadFiles(readFiles);

                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message);

                    this.Invoke(new MethodInvoker(
                       delegate()
                       {
                           receivingProgressForm.Close();
                           button_fileReceive.Enabled = false;
                           button_deletefile.Enabled = false;
                       }
                    )
                );
                }


            }
            else
            {

                MessageBox.Show("The connected device has no data");
            }
        }

        private static List<byte> readSerialPacket(System.IO.Ports.SerialPort serialPort1)
        {
            //Thread.Sleep(500);
            List<byte> result = new List<byte>();

            byte b = (byte)serialPort1.ReadByte();


            while (b != Protocol.PC_COMM_START)
            {
                b = (byte)serialPort1.ReadByte();

            }

            b = (byte)serialPort1.ReadByte();



            while (b != Protocol.PC_COMM_END)
            {
                if (b != Protocol.PC_COMM_EXCEPTION)
                {
                    result.Add(b);
                }
                else
                {
                    byte c = (byte)serialPort1.ReadByte();


                    if (c == Protocol.PC_COMM_EXP_START)
                    {
                        result.Add(Protocol.PC_COMM_START);
                    }
                    else if (c == Protocol.PC_COMM_EXP_END)
                    {
                        result.Add(Protocol.PC_COMM_END);
                    }
                    else if (c == Protocol.PC_COMM_EXP_EXP)
                    {
                        result.Add(Protocol.PC_COMM_EXCEPTION);
                    }
                    else
                    {
                        result.Clear();
                        return result;
                    }
                }
                b = (byte)serialPort1.ReadByte();

            }

            return result;
        }

        void ConnectedDevice_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("ProgressMAX"))
            {
                if (receivingProgressForm != null)
                {
                    receivingProgressForm.SetPrograssMAX(ConnectedDevice.ProgressMAX);
                }
            }
            else if (e.PropertyName.Equals("ProgressValue"))
            {
                if (receivingProgressForm != null)
                {
                    receivingProgressForm.SetPrograssValue(ConnectedDevice.ProgressValue);
                }
            }
            else if (e.PropertyName.Equals("FileNum"))
            {
                if (receivingProgressForm != null)
                {
                    receivingProgressForm.SetFileIndex(ConnectedDevice.FileNum);
                }
            }
        }
        private void DeleteData()
        {

            if (ConnectedDevice.FirmwareVersion.MajorVersion == 2)
            {
                if (receivingProgressForm == null)
                {
                    receivingProgressForm = new Form_DataReceiveingPrograss(100);
                    this.Invoke((MethodInvoker)delegate
                    {
                        receivingProgressForm.Show();
                        receivingProgressForm.Text = "delete all data";
                    });
                    receivingProgressForm.SetPrograssValue(0);

                    ConnectedDevice.DeleteFiles();

                }
                else
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        receivingProgressForm.Show();
                        receivingProgressForm.Text = "delete all data";
                    });

                    receivingProgressForm.SetPrograssValue(0);
                    receivingProgressForm.SetPrograssMAX(100);
                    ConnectedDevice.DeleteFiles();
                }
            }
            else
            {
                ConnectedDevice.DeleteFiles();
            }
            button_fileReceive.Enabled = false;
            button_deletefile.Enabled = false;
        }
        public bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {

            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream =
                   new System.IO.FileStream(_FileName, System.IO.FileMode.Create,
                                            System.IO.FileAccess.Write);
                // Writes a block of bytes to this stream using data from
                // a byte array.
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);

                // close file stream
                _FileStream.Close();

                return true;
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}",
                                  _Exception.ToString());
            }

            // error occured, return false
            return false;
        }
        void ConnectedDevice_ReadDataCompleted(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                button2.Enabled = true;
                return;
            });
            ReadDataCompletedEventArg arg = (ReadDataCompletedEventArg)e;

            //전송 진행창이 있으면 닫음.

            if (receivingProgressForm != null)
            {
                receivingProgressForm.BeginInvoke(new MethodInvoker(receivingProgressForm.Close));
                receivingProgressForm = null;
            }

            //if (requestedFilesInfo.Count() != arg.files.Count)
            //{
            //    MessageBox.Show("파일 받기에 실패하였습니다. 다시 시도해 주십시오.");
            //    return;
            //}
            //else
            //{

            //}

            // MessageBox.Show("데이터 전송이 완료되었습니다.", "데이터 전송 완료", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (arg.files.Count == 0)
            {
                MessageBox.Show("전송된 파일이 없습니다. ");
                return;
            }

            this.Invoke((MethodInvoker)delegate
            {
                List<FileInfoToRead> errorList = new List<FileInfoToRead>();
                for (int i = 0; i < arg.files.Count; i++)
                {

                    if (arg.files[i].bytes.Count() != requestedFilesInfo[i].size)
                    {
                        if (arg.files[i].bytes.Count() == requestedFilesInfo[i].size + 10 && ((requestedFilesInfo[i].size % 256 == 0) && (requestedFilesInfo[i].size % 4096 != 0)))
                        {
                            arg.files[i].bytes = arg.files[i].bytes.Take(requestedFilesInfo[i].size).ToArray();
                        }
                        else if (requestedFilesInfo[i].size - arg.files[i].bytes.Count() < 4096)
                        {
                            //4096보다 차이가 적음.
                            Console.WriteLine("차이가 4096보다 작아서 일단 패스");
                            ;
                        }
                        else
                        {
                            FileInfoToRead temp = new FileInfoToRead(requestedFilesInfo[i].index, requestedFilesInfo[i].size);
                            temp.bytes = arg.files[i].bytes;
                            MessageBox.Show("size diff:" + (arg.files[i].bytes.Count() - requestedFilesInfo[i].size));
                            errorList.Add(temp);
                        }
                    }
                }
                if (errorList.Count > 0)
                {
                    string errorNumbers = "";
                    foreach (var temp in errorList)
                    {
                        errorNumbers += "" + temp.index + ", ";
                    }
                    errorNumbers += "a";
                    errorNumbers = errorNumbers.Replace(", a", " ");

#if DEBUG
                    System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
                    dialog.Title = "오류 파일 저장";

                    dialog.Filter = "9 axis save files (*.f9x)|*.f9x";
                    dialog.RestoreDirectory = true;
                    dialog.FileName = ConnectedDevice.DeviceID + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string filename = dialog.FileName;
                        string newfilename = Path.GetDirectoryName(filename) + "\\" + Path.GetFileNameWithoutExtension(filename);
                        for (int i = 0; i < errorList.Count; i++)
                        {
                            if (errorList.Count == 1)
                            {
                                ByteArrayToFile(filename, errorList[i].bytes);
                            }
                            else
                            {
                                ByteArrayToFile(newfilename + "_" + i + ".f9x", errorList[i].bytes);
                            }
                        }
                    }

#endif
                    DialogResult result = MessageBox.Show("다음 파일 받기가 실패하였습니다. \n\nIndex: " + errorNumbers + "\n\n 다시 받으시겠습니까?", "파일전송 실패", MessageBoxButtons.RetryCancel);
                    if (result == System.Windows.Forms.DialogResult.Retry)
                    {
                        if (receivingProgressForm == null) receivingProgressForm = new Form_DataReceiveingPrograss();

                        receivingProgressForm.Text = "read file";
                        receivingProgressForm.Owner = this;
                        receivingProgressForm.TopLevel = true;

                        receivingProgressForm.Show();

                        ConnectedDevice.ReadFiles(requestedFilesInfo);
                        return;
                    }
                    else
                    {
                        return;
                    }
                }

#if ERRORCHECK
                for (int i = 0; i < arg.files.Count; i++)
                {
                    if (isValidFile(arg.files[i].bytes))
                    {
                        Console.WriteLine("유효한 파일");
                    }
                    else
                    {
                        MessageBox.Show("유효하지 않은 파일");
                        if (saveFilePath == null)
                        {
                            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();

                            dialog.Filter = "9 axis save files (*.f9x)|*.f9x";
                            dialog.RestoreDirectory = true;
                            dialog.FileName = ConnectedDevice.DeviceID + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                string filename = dialog.FileName;
                                string newfilename = Path.GetDirectoryName(filename) + "\\" + Path.GetFileNameWithoutExtension(filename);
                                ByteArrayToFile(newfilename + "_" + arg.files[i].index + ".f9x", arg.files[i].bytes);
                            }
                        }
                    }
                }
                
             //   return;

#endif

                if (saveFilePath == null)
                {
                    System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();

                    dialog.Filter = "9 axis save files (*.f9x)|*.f9x";
                    dialog.RestoreDirectory = true;
                    dialog.FileName = ConnectedDevice.DeviceID + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string filename = dialog.FileName;
                        string newfilename = Path.GetDirectoryName(filename) + "\\" + Path.GetFileNameWithoutExtension(filename);
                        for (int i = 0; i < arg.files.Count; i++)
                        {
                            ByteArrayToFile(newfilename + "_" + arg.files[i].index + ".f9x", arg.files[i].bytes);
                        }
                    }
                }
                else
                {
                    string filename = saveFilePath;
                    string newfilename = Path.GetDirectoryName(filename) + "\\" + Path.GetFileNameWithoutExtension(filename);
                    for (int i = 0; i < arg.files.Count; i++)
                    {
                        if (arg.files.Count == 1)
                        {
                            ByteArrayToFile(filename, arg.files[i].bytes);
                        }
                        else
                        {
                            ByteArrayToFile(newfilename + "_" + i + ".f9x", arg.files[i].bytes);
                        }
                    }
                }




                return;
            });
        }
        void ConnectedDevice_DeleteFilesCompleted(object sender, EventArgs e)
        {
            if (receivingProgressForm != null)
            {
                //note that the form might be closed 
                //before the maximum of the prgbar is reached

                if (receivingProgressForm != null)
                {
                    receivingProgressForm.BeginInvoke(new MethodInvoker(receivingProgressForm.Close));
                    receivingProgressForm = null;
                }
            }

#if AXIS_9
            MessageBox.Show("파일 삭제 성공");
#endif
        }

#if SPEEDTEST
        SerialClient fastSerialProt1;
        DateTime speedTest_start;
        int speedTest_receivedCount;
       
#endif
        private void DeviceConnectProcess()
        {

            if (ConnectedDevice != null && ConnectedDevice.HasData)
            {
                if (receivingProgressForm == null) receivingProgressForm = new Form_DataReceiveingPrograss();

                receivingProgressForm.Text = "read file";
                receivingProgressForm.Owner = this;
                receivingProgressForm.TopLevel = true;

                this.BeginInvoke(new MethodInvoker(receivingProgressForm.Show));

                Thread.Sleep(1000);
                try
                {
                    List<int> fileSizes = ConnectedDevice.getAllFileInfo_v2();
                    requestedFilesInfo = new List<FileInfoToRead>();
                    int i = 0;
                    foreach (var temp in fileSizes)
                    {
                        requestedFilesInfo.Add(new FileInfoToRead(i, temp));


                        i++;
                    }
                    ConnectedDevice.ReadAllFiles();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);

                    this.Invoke(new MethodInvoker(
                       delegate()
                       {
                           receivingProgressForm.Close();
                           button_fileReceive.Enabled = false;
                           button_deletefile.Enabled = false;
                       }
                    )
                );
                }


            }
            else
            {

                MessageBox.Show("The connected device has no data");
            }
        }

        SerialPort serialPort;
        void SerialPortService_PortsChanged(object sender, PortsChangedArgs e)
        {
#if SPEEDTEST
            if (e.EventType == EventType.Insertion)
            {
                serialPort= new SerialPort(e.ChangedPort, 499968);
                serialPort.Open();
                fastSerialProt1 = new SerialClient(serialPort, 1000000000);
               
                fastSerialProt1.OnReceiving +=fastSerialProt1_OnReceiving;
                fastSerialProt1.OnReadComplete += fastSerialProt1_OnReadComplete;
                fastSerialProt1.StartToRead_TEST();
                speedTest_start = DateTime.UtcNow;
                speedTest_receivedCount = 0;
                
            }
            else
            {
                if (fastSerialProt1 != null)
                {
                    fastSerialProt1.stopRead();
                    fastSerialProt1 = null;

                }
                
            }
            return;
#endif
            if (e.EventType == EventType.Insertion)
            {
                Thread.Sleep(1000);

                if (Fitmeter.IsFitmeter(e.ChangedPort))
                {
                    if (ConnectedDevice == null)
                    {
                        ConnectedDevice = new Fitmeter(e.ChangedPort);
                        //if (!ConnectedDevice.DeviceID.StartsWith("1311"))
                        //{
                        //    ConnectedDevice.IsConnected = false;
                        //    ConnectedDevice.Rejection();

                        //    ConnectedDevice = null;
                        //    return;
                        //}
                        Console.WriteLine(ConnectedDevice.DeviceID);
                        ConnectedDevice.PropertyChanged += ConnectedDevice_PropertyChanged;
                        ConnectedDevice.ReadDataCompleted += ConnectedDevice_ReadDataCompleted;
                        ConnectedDevice.DeleteFilesCompleted += ConnectedDevice_DeleteFilesCompleted;
                        this.Invoke(new MethodInvoker(
                               delegate()
                               {
                                   button2.Enabled = false;
                               }
                            )
                        );
                        List<int> fileSizes = ConnectedDevice.getAllFileInfo_v2();

                        this.Invoke(new MethodInvoker(
                            delegate()
                            {
                                button2.Enabled = true;
                                this.groupBox_connectedDevice.Enabled = true;
                                this.label_serialNumber.Text = ConnectedDevice.DeviceID + (" v" + ConnectedDevice.FirmwareVersion.MajorVersion + "." + ConnectedDevice.FirmwareVersion.MinorVersion);

                                this.label_timeSetting.Text = "";
                                if (ConnectedDevice.HasData == true)
                                {
                                    button_fileReceive.Enabled = true;
                                    button_deletefile.Enabled = true;
                                }
                                else
                                {
                                    button_fileReceive.Enabled = false;
                                    button_deletefile.Enabled = false;
                                }

                                int i = 0;
                                listView1.Items.Clear();

                                if (fileSizes.Count > 0)
                                {
                                    groupBox1.Enabled = true;

                                }
                                else
                                {
                                    groupBox1.Enabled = false;
                                }
                                foreach (var temp in fileSizes)
                                {
                                    addFileInfo(i, temp);
                                    i++;
                                }
                            }
                         )
                     );
                    }
                    else
                    {
                        bool success = ConnectedDevice.refreshDeviceInfo();
                        if (success)
                        {
                            ConnectedDevice.StartBatteryCheck();
                        }
                    }
#if RECEIVER
                    Thread t1 = new Thread(new ThreadStart(DeviceConnectProcess));
                    t1.Start();
#endif

#if TIMESYNC
                   
                    

                    Thread t1 = new Thread(new ThreadStart(TimeSyncfunc));
                    //t1.Priority = ThreadPriority.Highest;
                    t1.Start();
#endif

#if AXIS_9
                    //Thread t1 = new Thread(new ThreadStart(TimeSyncfunc));
                    //t1.Start();
#endif
                }
            }
            else
            {
                if (ConnectedDevice != null && ConnectedDevice.PortNum != null && ConnectedDevice.PortNum.Equals(e.ChangedPort))
                {

                    ConnectedDevice.IsConnected = false;
                    ConnectedDevice.Rejection();

                    ConnectedDevice = null;
                    this.Invoke(new MethodInvoker(
                            delegate()
                            {
                                this.groupBox_connectedDevice.Enabled = false;
                                this.label_serialNumber.Text = "";
                                this.label_timeSetting.Text = "";
                                button_fileReceive.Enabled = false;
                                button_deletefile.Enabled = false;
                                listView1.Items.Clear();
                                groupBox1.Enabled = false;
                            }
                         ));
                }
            }
        }

        private void fastSerialProt1_OnReadComplete(object sender, FileReadComplteEventArgs e)
        {

#if SPEEDTEST
            
            fastSerialProt1.Dispose();

            this.Invoke(new MethodInvoker(
                    delegate()
                    {
                        SaveFileDialog dialog = new SaveFileDialog();

                        DialogResult result = dialog.ShowDialog();

                        if (result == System.Windows.Forms.DialogResult.OK)
                        {
                            ByteArrayToFile(dialog.FileName, e.result.ToArray());
                        }
                    }
                 )
             );
            //SaveFileDialog dialog = new SaveFileDialog();
            
            //DialogResult result = dialog.ShowDialog();
            
            //if (result == System.Windows.Forms.DialogResult.OK)
            //{
            //    ByteArrayToFile(dialog.FileName,e.result.ToArray());
            //}

#endif
        }
#if SPEEDTEST

        private void fastSerialProt1_OnReceiving(object sender, DataReceived e)
        {
            speedTest_receivedCount = e.readData;
            TimeSpan ts = new TimeSpan(DateTime.UtcNow.Ticks - speedTest_start.Ticks);
            this.Invoke(new MethodInvoker(
                    delegate()
                    {
                        label_time.Text = "" + Math.Round(speedTest_receivedCount / ts.TotalSeconds, 2) + " BPS.";
                    }
                 )
             );
            Console.WriteLine(Math.Round(speedTest_receivedCount/1000 / ts.TotalSeconds, 2) + " KByte/s");
        }
#endif
        Multimedia.Timer mmTimer;
        private DateTime previous;

        private void TimeSyncfunc()
        {
            this.Invoke(new MethodInvoker(
                    delegate()
                    {
                        this.label_timeSetting.Text = " 동기화를 진행중입니다... ";
                        this.button2.Enabled = false;

                    }
                 )
             );
            mmTimer = new Multimedia.Timer();

            mmTimer.Mode = TimerMode.Periodic;
            mmTimer.Period = 1;
            mmTimer.Resolution = 1;

            previous = TimeZone.CurrentTimeZone.ToLocalTime(DateTime.UtcNow); ;

            mmTimer.Start();

            mmTimer.Tick += mmTimer_Tick;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            FitmeterPortWatcher.CleanUp();
        }

        private void mmTimer_Tick(object sender, System.EventArgs e)
        {

            DateTime datetime = TimeZone.CurrentTimeZone.ToLocalTime(DateTime.UtcNow);
            //Console.WriteLine(datetime.ToString());
            if (previous.Second != datetime.Second)
            {
                //시간 설정
                mmTimer.Tick -= mmTimer_Tick;
                //타이머 종료
                mmTimer.Stop();
                Thread t1 = new Thread(new ThreadStart(setTime));
                t1.Priority = ThreadPriority.Highest;
                t1.Start();
                mmTimer.Dispose();
            }
            else
            {
                previous = datetime;
            }
        }



        private void setTime()
        {
            //DateTime first = DateTime.Now;
            //ConnectedDevice.setTime(DateTime.Now);
            //DateTime second = DateTime.Now;

            //Console.WriteLine("시작:" + first.ToString("HH-mm-ss.fff"));
            //Console.WriteLine("종료:" + second.ToString("HH-mm-ss.fff"));

            DateTime pre_dt = DateTime.UtcNow;
            bool success = ConnectedDevice.setTime(TimeZone.CurrentTimeZone.ToLocalTime(DateTime.UtcNow));
            DateTime after_dt = DateTime.UtcNow;



            Console.WriteLine("duration is " + new TimeSpan(after_dt.Ticks - pre_dt.Ticks).TotalMilliseconds / 1000);
            this.Invoke(new MethodInvoker(
                    delegate()
                    {
                        this.button2.Enabled = true;
                        if (success)
                        {
                            this.label_timeSetting.Text = "성공";
                        }
                        else
                        {
                            this.label_timeSetting.Text = "실패";
                        }

                    }
                 )
             );
        }

        private void button_deletefile_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("연결된 기기의 모든 파일을 삭제합니다. 삭제한 파일은 복구가 불가능합니다.\n 삭제 하시겠습니까?", "파일 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                DeleteData();
                listView1.Items.Clear();
                groupBox1.Enabled = false;
            }

        }

        private void button_fileReceive_Click(object sender, EventArgs e)
        {
            if (!getSaveFilePath())
            {
                return;
            }
            List<FileInfoToRead> toRead = new List<FileInfoToRead>();
            foreach (ListViewItem temp in listView1.Items)
            {
                int index = Int32.Parse(temp.Text);
                int size = Int32.Parse(temp.SubItems[1].Text);
                if (size != 0) toRead.Add(new FileInfoToRead(index, size));


            }

            if (toRead.Count > 0)
            {
                if (receivingProgressForm == null) receivingProgressForm = new Form_DataReceiveingPrograss();
                receivingProgressForm.Text = "read file";
                receivingProgressForm.Owner = this;
                receivingProgressForm.TopLevel = true;

                receivingProgressForm.Show();
            }

            readFiles(toRead);
        }



        private void button2_Click(object sender, EventArgs e)
        {
            TimeSyncfunc();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "csv로 내보낼 파일을 선택하여 주십시오.";
            dialog.Filter = "9 axis save files (*.f9x)|*.f9x";
            dialog.Multiselect = true;
            dialog.RestoreDirectory = true;

            DialogResult result = dialog.ShowDialog();
            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            List<List<byte>> fileData = new List<List<byte>>();
            foreach (var tempFileName in dialog.FileNames)
            {
                BinaryReader sr = new BinaryReader(File.Open(tempFileName, FileMode.Open));


                int length = (int)sr.BaseStream.Length;
                List<byte> fileBytes = new List<byte>();
                for (int k = 0; k < length; k++)
                {
                    int temp = sr.ReadByte();
                    fileBytes.Add((byte)(temp));
                }
                fileData.Add(fileBytes);
                sr.Close();
            }


            foreach (var file in fileData)
            {
                if (file.Count < 62)
                {
                    MessageBox.Show("파일크기는 최소한 62byte를 넘어야 합니다. 적합한 파일이 아닙니다.");
                    return;
                }
            }


            Form1 form1 = this;

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.Cursor = Cursors.WaitCursor;

                new Thread(delegate()
                {
                    try
                    {
                        int count = 0;
                        foreach (var temp in dialog.FileNames)
                        {
                            string fileNameOfFileterdCSV = Path.GetDirectoryName(temp) + "\\" + Path.GetFileNameWithoutExtension(temp);
                            WriteCSV(fileData[count], fileNameOfFileterdCSV + ".csv");

                            fileNameOfFileterdCSV = fileNameOfFileterdCSV + "_Filtered.csv";

                            WriteCSV(fileData[count], fileNameOfFileterdCSV, true);
                            count++;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    }
                    form1.Invoke(new MethodInvoker(
                       delegate()
                       {
                           form1.Cursor = null;
                       }));
                    
                }).Start();
                
            }
        }

        private void getCalibrationValues(double inputRTCClock, out int CALM, out bool CALP)
        {
            CALM = 0;
            const int clock = 32768;
            double rate = clock/inputRTCClock;
            int twoPower20=1048576;
           // double diff = 32768 - inputRTCClock;

            if (inputRTCClock >= clock)
            {
                CALP = true;

                CALM = (int)Math.Round((twoPower20 * (1 - rate) / (1 + rate))); 
            }
            else
            {
                CALP = false;
                CALM = (int)Math.Round( 512 - (rate - 1) * twoPower20 / rate);
            }
            return;
        }

        private void WriteCSV(List<byte> readBytes, string path, bool isFiltered = false)
        {
            string fileName = path;
            if (!fileName.Contains(".csv"))
            {
                fileName = fileName + ".csv";
            }
            if (File.Exists(fileName))
            {
                DialogResult result = MessageBox.Show("이미 같은 이름의 파일이 있습니다. 기존 파일을 덮어씁니다. 계속 하시겠습니까?", "이미 존재하는 파일명", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                if (result == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }

            }
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(fileName, false, System.Text.Encoding.Default))
                {
                    int index = WriteCSVHeader(readBytes, streamWriter);
                    if (!isFiltered)
                    {
                        streamWriter.WriteLine("Index,Accel_X,Accel_Y,Accel_Z, Gyro_X, Gyro_Y, Gyro_Z, Magnet_X, Magnet_Y, Magnet_Z, Button Pushed");
                    }
                    else
                    {
                        streamWriter.WriteLine("Index,Accel_FX,Accel_FY,Accel_FZ, Gyro_X, Gyro_Y, Gyro_Z, Magnet_X, Magnet_Y, Magnet_Z, Button Pushed");
                    }

#if MAGNETFILTER
                     WriteCSVData_V2_MagnetFilter(readBytes, streamWriter, index, isFiltered);
                   
#else
                    WriteCSVData_V2(readBytes, streamWriter, index, isFiltered);
#endif
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("파일 저장에 실패하였습니다. 저장 하려는 파일이 이미 열려 있는 것 같습니다." + e.ToString(), "파일 저장 실패", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool checkCounter(int count, byte firstAccelByte)
        {
            int k = (firstAccelByte & 0xf0) >> 4;

            if (k == count) return true;
            return false;
        }
        private bool isValidFile2(byte[] readBytes)
        {
            
            int counter = 0;

            
            try
            {
                int offset = 62;
                int length = readBytes.Length;

                counter = (readBytes[offset + 13] << 24) + (readBytes[offset + 14] << 16) + (readBytes[offset + 15] << 8) + (readBytes[offset + 16]);
                offset += 17;
                while (offset < length)
                {

                    if (offset + 17 >= length)
                    {
                        return true;

                    }

                    if (readBytes[offset] == 0xff)
                    {
                        for (int i = 1; i < 17; i++)
                        {
                            if (readBytes[offset + i] != 0xff)
                            {
                                Console.WriteLine("시간 체크 부분이 모두 17byte모두 0xff여야 합니다.");
                                return false;
                            }
                        }
                        offset += 17;
                        continue;
                    }

                    counter++;
                    int realCount = 0;
                    if (!checkCounter(counter, readBytes, offset, out realCount))
                    {
                        Console.WriteLine("형식에 어긋나게 저장되었습니다. 카운트가 맞지 않습니다.");
                       
                        return false;
                    }

                   


                    offset += 17;




                }
                return true;



            }
            catch (Exception err)
            {
                Console.WriteLine("파일을 읽는 중(데이터 영역)에 에러가 발생하였습니다." + err.StackTrace);
                return false;
            }
        }

        private bool checkCounter(int counter, byte[] readBytes, int offset, out int realCount)
        {
            realCount = (readBytes[offset + 13] << 24) + (readBytes[offset + 14] << 16) + (readBytes[offset + 15] << 8) + (readBytes[offset + 16]);

            if (realCount == counter) return true;
            else return false;
        }

        private bool isValidFile(byte[] readBytes)
        {
            Queue<CounterAndByte> queue = new Queue<CounterAndByte>();
            int counter = 0;

            for (int i = 0; i < 15; i++)
            {
                queue.Enqueue(new CounterAndByte(i, (byte)i));
            }

            try
            {
                int offset = 62;
                int length = readBytes.Length;
               
               
                while (offset < length)
                {

                    if (offset + 17 >= length)
                    {
                        return true;
                       
                    }

                    if (readBytes[offset] == 0xff)
                    {  
                        for (int i = 1; i < 17; i++)
                        {
                            if (readBytes[offset + i] != 0xff)
                            {
                                Console.WriteLine("시간 체크 부분이 모두 17byte모두 0xff여야 합니다. offset:" + offset);
                                return false;
                            }
                        }
                        offset += 17;
                        continue;
                    }
                    queue.Dequeue();
                    queue.Enqueue(new CounterAndByte(counter, readBytes[offset]));
                  
                    if (!checkCounter(counter, readBytes[offset]))
                    {
                        Console.WriteLine("형식에 어긋나게 저장되었습니다. 카운트가 맞지 않습니다.");
                        while (queue.Count > 0)
                        {
                            CounterAndByte v = queue.Dequeue();
                            Console.WriteLine(v.ToString());
                        }
                        return false;
                    }

                    counter++;
                    if (counter == 0x0f) counter = 0;
                  
                  
                    offset += 17;
                   

                   
                    
                }
                return true;
                  


            }
            catch (Exception err)
            {
                Console.WriteLine("파일을 읽는 중(데이터 영역)에 에러가 발생하였습니다." + err.StackTrace);
                return false;
            }
        }

        private void WriteCSVData_V2(List<byte> readBytes, StreamWriter streamWriter, int offsetIndex, bool isFiltered = false)
        {

            int counter = 0;
            int index = 0;
            try
            {
                int offset = offsetIndex;
                int length = readBytes.Count;
                if (isFiltered)
                {

                    bool first = true;
                    AccelFilter filter = null;
                    bool TimeCheck = false;
                    while (offset < length)
                    {

                        if (offset + 17 >= length)
                        {

                            break;
                        }

                        if (readBytes[offset] == 0xff)
                        {
                            TimeCheck = true;
                            for (int i = 1; i < 16; i++)
                            {
                                if (readBytes[offset + i] != 0xff)
                                {
                                    throw new Exception("시간 체크 부분이 모두 17byte모두 0xff여야 합니다.");
                                }
                            }
                            offset += 17;

                        }
                        else
                        {
                            if (!checkCounter(counter, readBytes[offset]))
                            {
                                throw new Exception("형식에 어긋나게 저장되었습니다. 카운트가 맞지 않습니다.");
                            }
                            counter++;
                            if (counter == 0x0f) counter = 0;
                            short[] accel = new short[3];
                            short[] gyro = new short[3];
                            short[] magnet = new short[3];

                            byte[] accelBytes = new byte[5] { readBytes[offset], readBytes[offset + 1], readBytes[offset + 2], readBytes[offset + 3], readBytes[offset + 4] };
                            accel = getAxisValues(accelBytes);

                            if (first)
                            {
                                filter = new AccelFilter(accel);
                                first = false;
                            }
                            accel = filter.getFilteredAccelValues(accel);
                            offset += 5;
                            gyro[0] = toShortFromBytes(readBytes[offset], readBytes[offset + 1]);
                            gyro[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                            gyro[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);
                            offset += 6;
                            magnet[0] = toShortFromBytes(readBytes[offset], readBytes[offset + 1]);
                            magnet[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                            magnet[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);

                            magnet[0] = (short)(magnet[0] * 1300 / 2500);
                            magnet[1] = (short)(magnet[1] * 1300 / 2500);

                            offset += 6;
                            if (TimeCheck)
                            {
                                streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}, {10}", index, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2], "Pushed"));
                                TimeCheck = false;
                            }
                            else
                            {
                                streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", index, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2]));
                            }
                            index++;
                        }


                    }
                }
                else
                {
                    bool TimeCheck = false;
                    while (offset < length)
                    {

                        if (offset + 17 >= length)
                        {

                            break;
                        }

                        if (readBytes[offset] == 0xff)
                        {
                            TimeCheck = true;
                            for (int i = 1; i < 17; i++)
                            {
                                if (readBytes[offset + i] != 0xff)
                                {
                                    throw new Exception("시간 체크 부분이 모두 17byte모두 0xff여야 합니다. offset:" + offset);
                                }
                            }
                            offset += 17;
                            continue;
                        }
                        if (!checkCounter(counter, readBytes[offset]))
                        {
                            throw new Exception("형식에 어긋나게 저장되었습니다. 카운트가 맞지 않습니다. offset:" + offset);
                        }
                        counter++;
                        if (counter == 0x0f) counter = 0;
                        short[] accel = new short[3];
                        short[] gyro = new short[3];
                        short[] magnet = new short[3];

                        byte[] accelBytes = new byte[5] { readBytes[offset], readBytes[offset + 1], readBytes[offset + 2], readBytes[offset + 3], readBytes[offset + 4] };
                        accel = getAxisValues(accelBytes);

                        offset += 5;
                        gyro[0] = toShortFromBytes(readBytes[offset], readBytes[offset + 1]);
                        gyro[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                        gyro[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);
                        offset += 6;
                        magnet[0] = toShortFromBytes(readBytes[offset], readBytes[offset + 1]);
                        magnet[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                        magnet[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);

                        magnet[0] = (short)(magnet[0] * 1300 / 2500);
                        magnet[1] = (short)(magnet[1] * 1300 / 2500);

                        offset += 6;
                        if (TimeCheck)
                        {
                            streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}, {10}", index, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2], "Pushed"));
                            TimeCheck = false;
                        }
                        else
                        {
                            streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", index, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2]));
                        }
                        index++;
                    }
                }


            }
            catch (Exception err)
            {
                MessageBox.Show("파일을 읽는 중(데이터 영역)에 에러가 발생하였습니다." + err.ToString(), "파일 읽기 실패", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
            }
        }


        private void WriteCSVData_V2_MagnetFilter(List<byte> readBytes, StreamWriter streamWriter, int offsetIndex, bool isFiltered = false)
        {
            int index = 0;
            try
            {
                int offset = offsetIndex;
                int length = readBytes.Count;
                if (isFiltered)
                {
                    MagnetConpensation filter = new MagnetConpensation();
                    bool TimeCheck = false;
                    while (offset < length)
                    {

                        if (offset + 17 >= length)
                        {

                            break;
                        }

                        if (readBytes[offset] == 0xff)
                        {
                            TimeCheck = true;
                            for (int i = 1; i < 16; i++)
                            {
                                if (readBytes[offset + i] != 0xff)
                                {
                                    throw new Exception("시간 체크 부분이 모두 17byte모두 0xff여야 합니다.");
                                }
                            }
                            offset += 17;

                        }
                        else
                        {
                            short[] accel = new short[3];
                            short[] gyro = new short[3];
                            short[] magnet = new short[3];

                            byte[] accelBytes = new byte[5] { readBytes[offset], readBytes[offset + 1], readBytes[offset + 2], readBytes[offset + 3], readBytes[offset + 4] };
                            accel = getAxisValues(accelBytes);

                            offset += 5;
                            gyro[0] = toShortFromBytes(readBytes[offset], readBytes[offset + 1]);
                            gyro[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                            gyro[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);
                            offset += 6;
                            magnet[0] = toShortFromBytes(readBytes[offset], readBytes[offset + 1]);
                            magnet[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                            magnet[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);

                            magnet[0] = (short)(magnet[0] * 1300 / 2500);
                            magnet[1] = (short)(magnet[1] * 1300 / 2500);

                            accel = filter.getAccelValuesWithMagnetFilter(magnet, accel, 256);

                            offset += 6;
                            if (TimeCheck)
                            {
                                streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}, {10}", index, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2], "Pushed"));
                                TimeCheck = false;
                            }
                            else
                            {
                                streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", index, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2]));
                            }
                            index++;
                        }


                    }
                }
                else
                {
                    bool TimeCheck = false;
                    while (offset < length)
                    {

                        if (offset + 17 >= length)
                        {

                            break;
                        }

                        if (readBytes[offset] == 0xff)
                        {
                            TimeCheck = true;
                            for (int i = 1; i < 16; i++)
                            {
                                if (readBytes[offset + i] != 0xff)
                                {
                                    throw new Exception("시간 체크 부분이 모두 17byte모두 0xff여야 합니다.");
                                }
                            }
                            offset += 17;
                            continue;
                        }
                        short[] accel = new short[3];
                        short[] gyro = new short[3];
                        short[] magnet = new short[3];

                        byte[] accelBytes = new byte[5] { readBytes[offset], readBytes[offset + 1], readBytes[offset + 2], readBytes[offset + 3], readBytes[offset + 4] };
                        accel = getAxisValues(accelBytes);

                        offset += 5;
                        gyro[0] = toShortFromBytes(readBytes[offset], readBytes[offset + 1]);
                        gyro[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                        gyro[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);
                        offset += 6;
                        magnet[0] = toShortFromBytes(readBytes[offset], readBytes[offset + 1]);
                        magnet[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                        magnet[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);

                        magnet[0] = (short)(magnet[0] * 1300 / 2500);
                        magnet[1] = (short)(magnet[1] * 1300 / 2500);

                        offset += 6;
                        if (TimeCheck)
                        {
                            streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}, {10}", index, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2], "Pushed"));
                            TimeCheck = false;
                        }
                        else
                        {
                            streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", index, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2]));
                        }
                        index++;
                    }
                }


            }
            catch (Exception err)
            {
                MessageBox.Show("파일을 읽는 중(데이터 영역)에 에러가 발생하였습니다." + err.ToString(), "파일 읽기 실패", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
            }
        }

        /// <summary>
        /// 5byte를 전송받아 12bit짜리 각 축 데이터를 short배열로 리턴한다.
        /// </summary>
        /// <param name="accelBytes">반드시 5byte여야 한다.</param>
        /// <returns></returns>
        private short[] getAxisValues(byte[] accelBytes)
        {


            int x = ((0x0f & accelBytes[0]) << 8) + accelBytes[1];
            int y = (accelBytes[2] << 4);
            y += (accelBytes[3] & 0xf0) >> 4;
            int z = ((accelBytes[3] & 0x0f) << 8) + accelBytes[4];

            if ((accelBytes[0] & 0x08) > 0)
            {
                x -= 4096;
            }

            if ((accelBytes[2] & 0x80) > 0)
            {
                y -= 4096;
            }

            if ((accelBytes[3] & 0x08) > 0)
            {
                z -= 4096;
            }

            return new short[] { (short)x, (short)y, (short)z };


        }
        private void WriteCSVData_MagnetFilter(List<byte> readBytes, StreamWriter streamWriter, int offsetIndex, bool isFiltered = false)
        {
            int index = 0;
            try
            {
                int offset = offsetIndex;
                int length = readBytes.Count;
                if (isFiltered)
                {


                    MagnetConpensation conpensation = new MagnetConpensation();
                    while (offset < length)
                    {

                        if (offset + 18 >= length)
                        {
                            break;
                        }

                        short[] accel = new short[3];
                        short[] gyro = new short[3];
                        short[] magnet = new short[3];

                        accel[0] = toShortFromBytes(readBytes[offset + 0], readBytes[offset + 1]);
                        accel[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                        accel[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);


                        gyro[0] = toShortFromBytes(readBytes[offset + 6], readBytes[offset + 7]);
                        gyro[1] = toShortFromBytes(readBytes[offset + 8], readBytes[offset + 9]);
                        gyro[2] = toShortFromBytes(readBytes[offset + 10], readBytes[offset + 11]);
                        magnet[0] = toShortFromBytes(readBytes[offset + 12], readBytes[offset + 13]);
                        magnet[1] = toShortFromBytes(readBytes[offset + 14], readBytes[offset + 15]);
                        magnet[2] = toShortFromBytes(readBytes[offset + 16], readBytes[offset + 17]);
                        magnet[0] = (short)(magnet[0] * 1300 / 2500);
                        magnet[1] = (short)(magnet[1] * 1300 / 2500);

                        accel = conpensation.getAccelValuesWithMagnetFilter(magnet, accel, 256);

                        streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", index, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2]));

                        index++;
                        offset += 18;
                    }
                }
                else
                {
                    while (offset < length)
                    {

                        if (offset + 18 >= length)
                        {
                            break;
                        }

                        short[] gravity = new short[3];
                        short[] gyro = new short[3];
                        short[] magnet = new short[3];

                        gravity[0] = toShortFromBytes(readBytes[offset + 0], readBytes[offset + 1]);
                        gravity[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                        gravity[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);
                        gyro[0] = toShortFromBytes(readBytes[offset + 6], readBytes[offset + 7]);
                        gyro[1] = toShortFromBytes(readBytes[offset + 8], readBytes[offset + 9]);
                        gyro[2] = toShortFromBytes(readBytes[offset + 10], readBytes[offset + 11]);
                        magnet[0] = toShortFromBytes(readBytes[offset + 12], readBytes[offset + 13]);
                        magnet[1] = toShortFromBytes(readBytes[offset + 14], readBytes[offset + 15]);
                        magnet[2] = toShortFromBytes(readBytes[offset + 16], readBytes[offset + 17]);
                        magnet[0] = (short)(magnet[0] * 1300 / 2500);
                        magnet[1] = (short)(magnet[1] * 1300 / 2500);
                        streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", index, gravity[0], gravity[1], gravity[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2]));

                        index++;
                        offset += 18;
                    }
                }


            }
            catch (Exception err)
            {
                MessageBox.Show("파일을 읽는 중(데이터 영역)에 에러가 발생하였습니다." + err.ToString(), "파일 읽기 실패", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
            }
        }
        private void WriteCSVData(List<byte> readBytes, StreamWriter streamWriter, int offsetIndex, bool isFiltered = false)
        {
            int index = 0;
            try
            {
                int offset = offsetIndex;
                int length = readBytes.Count;
                if (isFiltered)
                {

                    bool first = true;
                    //   Gravityfilter filter = null;
                    AccelFilter filter = null;
                    while (offset < length)
                    {

                        if (offset + 18 >= length)
                        {
                            break;
                        }

                        short[] accel = new short[3];
                        short[] gyro = new short[3];
                        short[] magnet = new short[3];

                        accel[0] = toShortFromBytes(readBytes[offset + 0], readBytes[offset + 1]);
                        accel[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                        accel[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);
                        if (first)
                        {
                            // filter = new Gravityfilter(0.03f, 1/250.0f,  accel);
                            filter = new AccelFilter(accel, 0.3f, 250);
                            first = false;
                        }

                        accel = filter.getFilteredAccelValues(accel);
                        gyro[0] = toShortFromBytes(readBytes[offset + 6], readBytes[offset + 7]);
                        gyro[1] = toShortFromBytes(readBytes[offset + 8], readBytes[offset + 9]);
                        gyro[2] = toShortFromBytes(readBytes[offset + 10], readBytes[offset + 11]);
                        magnet[0] = toShortFromBytes(readBytes[offset + 12], readBytes[offset + 13]);
                        magnet[1] = toShortFromBytes(readBytes[offset + 14], readBytes[offset + 15]);
                        magnet[2] = toShortFromBytes(readBytes[offset + 16], readBytes[offset + 17]);

                        magnet[0] = (short)(magnet[0] * 1300 / 2500);
                        magnet[1] = (short)(magnet[1] * 1300 / 2500);

                        streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", index, accel[0], accel[1], accel[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2]));

                        index++;
                        offset += 18;
                    }
                }
                else
                {
                    while (offset < length)
                    {

                        if (offset + 18 >= length)
                        {
                            break;
                        }

                        short[] gravity = new short[3];
                        short[] gyro = new short[3];
                        short[] magnet = new short[3];

                        gravity[0] = toShortFromBytes(readBytes[offset + 0], readBytes[offset + 1]);
                        gravity[1] = toShortFromBytes(readBytes[offset + 2], readBytes[offset + 3]);
                        gravity[2] = toShortFromBytes(readBytes[offset + 4], readBytes[offset + 5]);
                        gyro[0] = toShortFromBytes(readBytes[offset + 6], readBytes[offset + 7]);
                        gyro[1] = toShortFromBytes(readBytes[offset + 8], readBytes[offset + 9]);
                        gyro[2] = toShortFromBytes(readBytes[offset + 10], readBytes[offset + 11]);
                        magnet[0] = toShortFromBytes(readBytes[offset + 12], readBytes[offset + 13]);
                        magnet[1] = toShortFromBytes(readBytes[offset + 14], readBytes[offset + 15]);
                        magnet[2] = toShortFromBytes(readBytes[offset + 16], readBytes[offset + 17]);

                        magnet[0] = (short)(magnet[0] * 1300 / 2500);
                        magnet[1] = (short)(magnet[1] * 1300 / 2500);

                        streamWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", index, gravity[0], gravity[1], gravity[2], gyro[0], gyro[1], gyro[2], magnet[0], magnet[1], magnet[2]));

                        index++;
                        offset += 18;
                    }
                }


            }
            catch (Exception err)
            {
                MessageBox.Show("파일을 읽는 중(데이터 영역)에 에러가 발생하였습니다." + err.ToString(), "파일 읽기 실패", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
            }
        }

        private short toShortFromBytes(byte byte1, byte byte2)
        {
            return BitConverter.ToInt16(new byte[] { byte2, byte1 }, 0);
        }
        private string initDeviceID(byte[] devCode)
        {
            byte[] first = new byte[4];
            byte[] second = new byte[3];
            byte[] third = new byte[3];

            for (int i = 0; i < 4; i++)
            {
                first[i] = devCode[i];
            }

            for (int i = 0; i < 3; i++)
            {
                second[i] = devCode[i + 4];
            }
            for (int i = 0; i < 3; i++)
            {
                third[i] = devCode[i + 7];
            }
            System.Text.Encoding enc = System.Text.Encoding.ASCII;

            string devid = enc.GetString(first) + "-" + enc.GetString(second) + "-" + enc.GetString(third);
            return devid;
        }

        private int WriteCSVHeader(List<byte> readBytes, StreamWriter streamWriter)
        {


            int index = 0;
            int fileVersion_major = readBytes[6];
            int fileVersion_minor = readBytes[7];
            index += 8;
            index += 3;
            byte[] devid = new byte[10];
            for (int i = 0; i < 10; i++)
            {
                devid[i] = readBytes[index + i];
            }
            index += 10;
            //GravityRanges range = (GravityRanges)readDataFromDevice[index];
            //if (range == GravityRanges.Gravity16) this.MaxGravity = 16;
            //else if (range == GravityRanges.Gravity8) this.MaxGravity = 8;
            //else if (range == GravityRanges.Gravity4) this.MaxGravity = 4;
            //else if (range == GravityRanges.Gravity2) this.MaxGravity = 2;
            index++;

            //this.NormalStateAlarm = false;
            index++;

            //TimeMark mark = (TimeMark)readDataFromDevice[index];
            //if (mark == TimeMark.SET) this.hasTimeMarkData = true;
            //else this.hasTimeMarkData = false;
            index++;

            //WearingLocation wear = (WearingLocation)readDataFromDevice[index];
            //if (wear == Protocol_v2.WearingLocation.WAIST) this.WearingLocation = WearAt.WAIST;
            //else if (wear == Protocol_v2.WearingLocation.WRIST) this.WearingLocation = WearAt.WRIST;
            //else if (wear == Protocol_v2.WearingLocation.ANKLE) this.WearingLocation = WearAt.ANKLE;
            //else if (wear == Protocol_v2.WearingLocation.UPPERARM) this.WearingLocation = WearAt.HUMORUS;
            //else this.WearingLocation = WearAt.WAIST;
            index++;


            #region 저장간격
            //byte[] dataIntervalArray = new byte[16];
            //Array.Copy(readDataFromDevice, index, dataIntervalArray, 0, 16);
            index += 16;

            // List<DataAndInterval> dataAndIntervals =getDataAndIntervals(dataIntervalArray);
            //DataSavingInterval interval = Protocol_v2.DataSavingInterval.TEN;
            //if (dataIntervalArray[0] != 0)
            //{
            //    this.isRawdata = true;
            //    interval = (Protocol_v2.DataSavingInterval)dataIntervalArray[0];
            //    this.isFilteredData = false;
            //}
            //else if (dataIntervalArray[1] != 0)
            //{
            //    this.isRawdata = true;
            //    interval = (Protocol_v2.DataSavingInterval)dataIntervalArray[1];
            //    this.isFilteredData = true;
            //}
            //else
            //{
            //    this.isRawdata = false;
            //    interval = (Protocol_v2.DataSavingInterval)dataIntervalArray[2];
            //    this.isFilteredData = true;
            //}

            //this.DataSavingInterval = 1;
            //switch (interval)
            //{
            //    case Protocol_v2.DataSavingInterval.NONE:
            //        //this.DataSavingInterval = 1;
            //        //break;
            //        throw new Exception("incomming file is wrong a file");
            //    case Protocol_v2.DataSavingInterval.ONE:
            //        this.DataSavingInterval = 1;
            //        break;
            //    case Protocol_v2.DataSavingInterval.FIVE:
            //        this.DataSavingInterval = 5;
            //        break;
            //    case Protocol_v2.DataSavingInterval.TEN:
            //        this.DataSavingInterval = 10;
            //        break;
            //    case Protocol_v2.DataSavingInterval.THIRTY:
            //        this.DataSavingInterval = 30;
            //        break;
            //    case Protocol_v2.DataSavingInterval.SIXTY:
            //        this.DataSavingInterval = 60;
            //        break;
            //    case Protocol_v2.DataSavingInterval.HERTZ_2:
            //        this.DataSavingInterval = 1.0 / 2;
            //        break;
            //    case Protocol_v2.DataSavingInterval.HERTZ_4:
            //        this.DataSavingInterval = 1.0 / 4;
            //        break;
            //    case Protocol_v2.DataSavingInterval.HERTZ_8:
            //        this.DataSavingInterval = 1.0 / 8;
            //        break;
            //    case Protocol_v2.DataSavingInterval.HERTZ_16:
            //        this.DataSavingInterval = 1.0 / 16;
            //        break;
            //    case Protocol_v2.DataSavingInterval.HERTZ_32:
            //        this.DataSavingInterval = 1.0 / 32;
            //        break;
            //    case Protocol_v2.DataSavingInterval.HERTZ_64:
            //        this.DataSavingInterval = 1.0 / 64;
            //        break;
            //    case Protocol_v2.DataSavingInterval.HERTZ_128:
            //        this.DataSavingInterval = 1.0 / 128;
            //        break;
            //    case Protocol_v2.DataSavingInterval.HERTZ_256:
            //        this.DataSavingInterval = 1.0 / 256;
            //        break;
            //    case Protocol_v2.DataSavingInterval.HERTZ_512:
            //        this.DataSavingInterval = 1.0 / 512;
            //        break;
            //    case Protocol_v2.DataSavingInterval.HERTZ_1024:
            //        this.DataSavingInterval = 1.0 / 1024;
            //        break;
            //    default:
            //        throw new Exception("incomming file is a wrong file");
            //}
            #endregion

            //this.hasIlluminanceData = false;
            //this.luxDataSavingInterval = 0;

            //this.Activity = new List<int>();

            //if (isRawData == true)
            //{
            //    if (!isFilteredData)
            //    {
            //        this.X = new List<short>();
            //        this.Y = new List<short>();
            //        this.Z = new List<short>();
            //    }
            //    this.FX = new List<short>();
            //    this.FY = new List<short>();
            //    this.FZ = new List<short>();
            //}

            //this.DeviceID = initDeviceID(devid);

            // index += 18 + 16 + 1;
            index++;

            DateTime StartTime = DateTime.Now;
            try
            {
                StartTime = new DateTime(2010 + (int)readBytes[index], (int)readBytes[index + 1], (int)readBytes[index + 2], (int)readBytes[index + 4], (int)readBytes[index + 5], (int)readBytes[index + 6]);
            }
            catch (Exception)
            {
                MessageBox.Show("저장된 파일의 시작시간이 잘못된 정보를 포함하고 있습니다.");
                //return;
            }

            index += 7;
            int expandHeaderSize = readBytes[index];
            index++;
            expandHeaderSize = expandHeaderSize + (readBytes[index] << 8);
            index++;

            #region 확장해더
            if (expandHeaderSize != 0)
            {
                int size = 0;

                while (size < expandHeaderSize)
                {
                    //보정값 설정 
                    if (readBytes[index] == 0xf3)
                    {
                        index++;
                        size += readBytes[index] + 2;
                    }
                    else if (readBytes[index] == 0xf2)
                    {
                        //사용자
                        index++;
                        size += readBytes[index] + 2;
                        int userreadBytesize = readBytes[index];


                        index++;
                        //  int id = BitConverter.ToInt32(readBytes, index);

                        index += 4;

                        int weight = (int)readBytes[index++];
                        int height = (int)readBytes[index++];
                        int age = (int)readBytes[index++];
                        Sex sex = (Sex)readBytes[index++];
                        byte algo = (byte)readBytes[index++];
                        //  string userName = "";
                        List<byte> nameByte = new List<byte>();

                        for (int i = 0; i < userreadBytesize - 9; i++)
                        {
                            nameByte.Add(readBytes[index++]);
                        }
                        // userName = Util.ByteArrayToStr(nameByte.ToArray());

                        //UserInfoInDevice temp = new UserInfoInDevice(id, age, sex, height, weight, userName, algo);
                        //this.userInfoInDevice = temp;
                    }
                    else throw new Exception("incomming file is wrong file");

                }
            }
            #endregion

            string devID = initDeviceID(devid); ;
            string gravitySetting = "8G";
            string gyroscopeSetting = "1000도/s";
            string magentSEtting = "2500uT";

            streamWriter.WriteLine("DeviceID," + devID);
            streamWriter.WriteLine("GravitySetting," + gravitySetting);
            streamWriter.WriteLine("GyroscopeSetting," + gyroscopeSetting);
            streamWriter.WriteLine("MagentSetting," + magentSEtting);
            streamWriter.WriteLine("StartTime, " + StartTime.ToString("yyyy-MM-dd HH:mm:ss"));
            streamWriter.WriteLine();

            return index;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void addFileInfo(int index, int size)
        {
            // 뷰모드 지정
            ListViewItem listViewItem = new ListViewItem("" + index);
            listViewItem.SubItems.Add("" + size);
            listViewItem.SubItems.Add("전송");

            listView1.Items.Add(listViewItem);

        }

        private List<FileInfoToRead> requestedFilesInfo;

        private bool getSaveFilePath()
        {
            saveFilePath = null;
            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();

            dialog.Filter = "9 axis save files (*.f9x)|*.f9x";
            dialog.RestoreDirectory = true;
            dialog.FileName = ConnectedDevice.DeviceID + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                saveFilePath = dialog.FileName;
                return true;
            }
            else
            {
                return false;
            }
        }

        private void readFiles()
        {


            if (!getSaveFilePath())
            {
                return;
            }

            if (ConnectedDevice != null && ConnectedDevice.HasData)
            {
                if (receivingProgressForm == null) receivingProgressForm = new Form_DataReceiveingPrograss();

                receivingProgressForm.Text = "read file";
                receivingProgressForm.Owner = this;
                receivingProgressForm.TopLevel = true;

                receivingProgressForm.Show();
                try
                {
                    List<FileInfoToRead> toRead = new List<FileInfoToRead>();
                    foreach (ListViewItem temp in listView1.SelectedItems)
                    {
                        int index = Int32.Parse(temp.Text);
                        int size = Int32.Parse(temp.SubItems[1].Text);

                        toRead.Add(new FileInfoToRead(index, size));

                    }
                    requestedFilesInfo = toRead;
                    ConnectedDevice.ReadFiles(toRead);

                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message);

                    this.Invoke(new MethodInvoker(
                       delegate()
                       {
                           receivingProgressForm.Close();
                           button_fileReceive.Enabled = false;
                           button_deletefile.Enabled = false;
                       }
                    )
                );
                }


            }
            else
            {

                MessageBox.Show("The connected device has no data");
            }
        }

        private void button_receiveSelectedFiles_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("선택된 파일이 없습니다.");
                return;
            }

            if (!getSaveFilePath())
            {
                return;
            }

            if (ConnectedDevice != null && ConnectedDevice.HasData)
            {
                if (receivingProgressForm == null) receivingProgressForm = new Form_DataReceiveingPrograss();

                receivingProgressForm.Text = "read file";
                receivingProgressForm.Owner = this;
                receivingProgressForm.TopLevel = true;

                receivingProgressForm.Show();
                try
                {
                    List<FileInfoToRead> toRead = new List<FileInfoToRead>();
                    foreach (ListViewItem temp in listView1.SelectedItems)
                    {
                        int index = Int32.Parse(temp.Text);
                        int size = Int32.Parse(temp.SubItems[1].Text);

                        toRead.Add(new FileInfoToRead(index, size));

                    }
                    requestedFilesInfo = toRead;

                    ConnectedDevice.ReadFiles(toRead);

                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message);

                    this.Invoke(new MethodInvoker(
                       delegate()
                       {
                           receivingProgressForm.Close();
                           button_fileReceive.Enabled = false;
                           button_deletefile.Enabled = false;
                       }
                    )
                );
                }


            }
            else
            {

                MessageBox.Show("The connected device has no data");
            }




        }

        private void readFiles(List<FileInfoToRead> toRead)
        {

            button2.Enabled = false;
            requestedFilesInfo = toRead;

            new Thread(delegate()
            {
                ConnectedDevice.ReadFiles(toRead);
            }).Start();

        }


        private void button3_Click(object sender, EventArgs e)
        {

            DialogResult result = MessageBox.Show("펌웨어 업데이트 모드로 진입하시겠습니까? 펌웨어 업데이트 모드로 진입하면 새 펌웨어를 업로드 하지 않으면 기기가 동작하지 않습니다.", "펌웨어 업데이트 모드로 진입", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                if (ConnectedDevice != null)
                {
                    bool go = ConnectedDevice.GoFirmwareUpdateMode();

                    if (go)
                    {
                       // MessageBox.Show("펌웨어 업데이트 모드 진입 성공.");
                        return;
                    }
                    else
                    {
                        MessageBox.Show("펌웨어 업데이트 모드 진입에 실패 하였습니다.");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("기기가 연결중이 아닙니다.");
                    return;
                }

            }
            else
            {
                return;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (ConnectedDevice != null)
            {
                DateTime dt = DateTime.Now;
                ConnectedDevice.getTimeInDevice(out dt);
                DateTime system = TimeZone.CurrentTimeZone.ToLocalTime(DateTime.UtcNow);
                label_time.Text = dt.ToString("HH-mm-ss");
                Console.WriteLine("Time diff:" + new TimeSpan(system.Ticks - dt.Ticks).ToString());
            }
            else
            {
                MessageBox.Show("장치가 연결중이 아닙니다.");
            }
        }


        private void button5_Click(object sender, EventArgs e)
        {
#if SPEEDTEST
            if (fastSerialProt1 != null)
            {
                fastSerialProt1.stopRead();
               
            }
#endif
        }

#if CALIB
        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text.Length == 0)
                {
                    MessageBox.Show("숫자를 입력하십시오.\n 예) 32768.21");
                    return;
                }
                double input = Convert.ToDouble(textBox1.Text);

                int CALM = 0;
                bool CALP = true;

                getCalibrationValues(input, out CALM, out CALP);

                Console.WriteLine(string.Format("input={0}, CALM={1}, CALP={2}", input, CALM, CALP?"true":"false"));

                if (CALM<0 || CALM > 512) {
                    MessageBox.Show("보정 한계치 초과");
                    return;
                }
                bool result = ConnectedDevice.doRTC_Calribration(input);

                if (result)
                {
                    label2.Text = "성공";
                    Console.WriteLine("RTC calb success");
                }
                else
                {
                    label2.Text = "실패";
                    Console.WriteLine("RTC calb falied");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("예외가 발생했습니다." + er.StackTrace);
            }
        }
#endif
        private void button7_Click(object sender, EventArgs e)
        {
            //배터리 체크
            if (ConnectedDevice != null)
            {
                int go = ConnectedDevice.ReadBatteryChargedRate();

                label_battery.Text = "" + go+"%";
            }
            else
            {
                MessageBox.Show("기기가 연결중이 아닙니다.");
                return;
            }
        }
    }

    public class CounterAndByte
    {
        private int count;
        private byte ByteValue;

        public CounterAndByte(int c, byte b)
        {
            count = c;
            ByteValue = b;

        }

        public override string ToString()
        {
            return "count:" + count + ", value:" + ByteValue;
        }
    }
   

}
    